#! /usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
import actionlib
import time
from relocation_service.srv import location_request, location_requestResponse

# Autor : Carlos Tortosa Mico
# Equipo : La Piña Bandida
# Proyecto ROBerto
# ------------------------------------------------

class Relocator(object):

    def __init__(self):
        rospy.init_node('relocation_node', anonymous=False)
        rospy.loginfo("Preparando servicio....")
        self.servicio = rospy.Service("/relocation_service", location_request, self.servicio_callback)
        self.moveBase = actionlib.SimpleActionClient("/move_base", MoveBaseAction)
        rospy.loginfo("Accediendo al topic move_base para navegar")
        self.moveBase.wait_for_server(rospy.Duration(15))


    def servicio_callback(self, request):
        rospy.loginfo("")
        rospy.loginfo("Request para relocalizar el robot:")
        coordX = request.coordX
        coordY = 1 - request.coordY
        rospy.loginfo("X:{}   Y:{}".format(coordX, coordY))

        response = location_requestResponse(rospy.Duration(10))

        try:
            self.enviar_goal(coordX, coordY)
            response.success = True

            rospy.loginfo("He alcanzado el lugar")
        except:
            response.success = False
            rospy.loginfo("Error enviando el goal")

        finally:
            
            return response
            

    def enviar_goal(self, x, y):

        mappedX, mappedY = self.map_coords(x,y)
        goal = MoveBaseGoal()

        rospy.loginfo("Me quiero dirigir a {} : {}".format(mappedX, mappedY))

        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.pose.position.x =  mappedX# Insertamos la coordenadas del goal
        goal.target_pose.pose.position.y = mappedY
        goal.target_pose.pose.position.z = 0
        goal.target_pose.pose.orientation.x = 0.0
        goal.target_pose.pose.orientation.y = 0
        goal.target_pose.pose.orientation.z = 0.1
        goal.target_pose.pose.orientation.w = 0

        self.moveBase.send_goal(goal)
        self.moveBase.wait_for_result()

        return

    def map_coords(self, x, y):
        # Coordenadas en el mapa de rviz
        max_X = 2.6
        max_Y = 4.1
        min_X = -2.55
        min_Y = -2.2

        
        nuevoX = (6.3 * x) + min_X
        nuevoY = (5.2 * y) + min_Y
        return (nuevoX, nuevoY)

        

relocator = Relocator()
rospy.spin()

    
