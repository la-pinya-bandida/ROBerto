#!/usr/bin/env python

# Autor: Victor Blanco Bataller

#Se usa cv_bridge para recibir las imagenes del topic sensor msgs/Image
#y OpenCV para convertir de RGB a HSV.
#Luego se aplica una mascara para que solo se mantenga la parte de la imagen amarilla
#asi cuando el robot ve la linea amarilla la sube


import rospy, cv2, cv_bridge, numpy
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist


import sys

from time import sleep


import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from pelota_msg.srv import MyCustomServiceMessage




class SeguidorPelota:



        def __init__(self,knownWidth, focalLength, distanciaBrazo):

                self.knownWidth=knownWidth
                self.focalLength=focalLength
                self.distaciaBrazo=distanciaBrazo
                self.i=0
                self.j=0
                self.bridge = cv_bridge.CvBridge()

                self.doNothing=True


                self.image_sub = None

                self.personaEnUltimoFrame=False

                self.distance=0



                self.cogidoPorBrazo=False

                self.cmd_vel_pub = rospy.Publisher('cmd_vel',
                        Twist, queue_size=1)
                self.twist = Twist()
                '''
                self._move_base = actionlib.SimpleActionClient("/move_base_simple", MoveBaseAction)
                rospy.loginfo("Accediendo al topic move_base para navegar")
                self._move_base.wait_for_server()
      

        def moverseHaciaPelotaADistancia(self):

                goal = MoveBaseGoal()  # Creamos el goal
                goal.target_pose.header.frame_id = 'base_link'
                goal.target_pose.pose.position.x = self.distance-self.distanciaBrazo  # Insertamos la coordenadas del goal
                #goal.target_pose.pose.position.y = self._position[1]

                goal.target_pose.pose.orientation.z = 0


                #rospy.loginfo("ROBOT %s" % (self._place))
                # sends the goal
                self._move_base.send_goal(goal)
                self._move_base.wait_for_result()
                #self.seHaMovido=True
            '''

        def cogerConBrazo(self):

                print("Cogiendo con brazo")

                rospy.sleep(3)

                self.cogidoPorBrazo = True


        def soltar(self):



                sleep(3)
                print("soltando carga")
                rospy.sleep(3)

                self.twist.linear.x = 0

                self.twist.angular.z = 0
                self.cmd_vel_pub.publish(self.twist)
                self.cogidoPorBrazo = False

        def girar(self):


                # obtenemos la altura, anchura y profundidad
                h, w, d = self.image.shape

                self.twist.angular.z=0.5
                self.cmd_vel_pub.publish(self.twist)


        def buscarPelota(self,w):

                self.i = 0
                self.j = 0
                hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
                rojo_bajo = (0, 186, 100)
                rojo_alto = (25, 255, 255)
                self.mask = cv2.inRange(hsv, rojo_bajo, rojo_alto)

                M = cv2.moments(self.mask, False)

                try:

                        marker = find_marker(self.image)
                        inches = distance_to_camera(self.knownWidth, self.focalLength, marker[1][0])
                        self.distance = (inches / 12) * 0.3048000097536
                        print(inches)
                        # draw a bounding box around the image and display it
                        # box = cv2.cv.BoxPoints(marker) if imutils.is_cv2() else cv2.boxPoints(marker)
                        # box = numpy.int0(box)
                        # cv2.drawContours(self.image, [box], -1, (0, 255, 0), 2)
                        cv2.putText(self.image, str((inches / 12) * 0.3048000097536) + "m",
                                    (self.image.shape[1] - 200, self.image.shape[0] - 20),
                                    cv2.FONT_HERSHEY_SIMPLEX,
                                    2.0, (0, 255, 0), 3)

                        cx = int(M['m10'] / M['m00'])
                        cy = int(M['m01'] / M['m00'])

                        # Se modifica el angulo del robot en funcion al angulo de la pelota respecto al centro de la imagen
                        err = cx - w / 2
                        self.twist.linear.x = 0.2

                        #self.twist.angular.z = 0
                        #self.cmd_vel_pub.publish(self.twist)

                        self.twist.angular.z = -float(err) / 100

                        # Se evitan velocidades angulares demasiado bruscas o imperceptibles

                        if self.twist.angular.z < 0.001:
                                self.twist.angular.z = 0
                        if self.twist.angular.z > 0.2:
                                self.twist.angular.z = 0.2

                        self.cmd_vel_pub.publish(self.twist)

                        # Cuando esta a dos o menos metros de la pelota va mas despacio

                        if (self.distance <= 2):
                                self.twist.linear.x = 0

                                self.cmd_vel_pub.publish(self.twist)

                                if (self.twist.angular.z < 0.01):

                                        # ahora es cuando publicamos un goal

                                        self.twist.angular.z = 0
                                        self.cmd_vel_pub.publish(self.twist)

                                        print("Ahora set goal")

                                        # si se encuentra a la distancia adecuada y no tiene nada cogido, coger pelota con el brazo, si no se sigue moviendo

                                        if (self.distance <= self.distaciaBrazo):

                                                self.twist.linear.x = 0

                                                self.cmd_vel_pub.publish(self.twist)

                                                self.cogerConBrazo()

                                        else:

                                                # self.moverseHaciaPelotaADistancia()
                                                self.twist.linear.x = 0.1

                                                self.cmd_vel_pub.publish(self.twist)

                                        # if(self.seHaMovido==False):
                                        #      self.moverseHaciaPelotaADistancia()

                except:
                        print("no bola")
                        self.twist.linear.x = 0

                        self.cmd_vel_pub.publish(self.twist)
                        # if(i<100):
                        self.girar()




        def buscarPersonas(self,w):
                # print("Cogido por brazo, buscando humanos")

                # definimos el clasificador
                cascada = cv2.CascadeClassifier(str(sys.path[0])[:-3] + "clasificadores/haarcascade_fullbody.xml")

                # Convertimos la imagen a escala de grises
                img_gris = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)

                cascada = cascada.detectMultiScale(img_gris, 1.1, 5)
                print(cascada)

                for x2, y2, w2, h2 in cascada:
                        cv2.rectangle(self.image, (x2, y2), (x2 + w2, y2 + h2), (255, 0, 0), 3)

                if (len(cascada) == 0):
                        self.j = self.j + 1

                        #si por lo menos han habido 30 frames seguios con persona y luego al menos 5 sin, ir soltando la carga
                        if (self.i > 20 and self.j > 5):
                                self.twist.linear.x = 0.2

                                self.twist.angular.z = 0
                                self.cmd_vel_pub.publish(self.twist)

                                self.soltar()

                        else:

                                self.girar()
                                if (self.i < 20):
                                        self.i = 0
                else:
                        err = x2/2 - w2 / 2
                        self.twist.linear.x = 0.2

                        self.twist.angular.z = 0
                        self.cmd_vel_pub.publish(self.twist)

                        self.twist.angular.z = -float(err) / 100

                        # Se evitan velocidades angulares demasiado bruscas o imperceptibles

                        if self.twist.angular.z < 0.0001:
                                self.twist.angular.z = 0
                        if self.twist.angular.z > 0.2:
                                self.twist.angular.z = 0.2

                        self.cmd_vel_pub.publish(self.twist)

                        self.i = self.i + 1
                        self.j = 0



        #callback que se llama cuando un nuevo frame es recibido

        def imagen_callback(self, msg):



                if(self.doNothing==False):

                        # Seleccionamos bgr8 porque es la codificacion de OpenCV por defecto

                        self.image = self.bridge.imgmsg_to_cv2(msg, desired_encoding="bgr8")

                        # obtenemos la altura, anchura y profundidad
                        h, w, d = self.image.shape

                        if (self.cogidoPorBrazo == False):

                                self.buscarPelota(w)


                        else:

                                self.buscarPersonas(w)


                        cv2.imshow("image", self.image)
                        cv2.waitKey(3)
                else:

                        cv2.destroyAllWindows()
                        cv2.waitKey(1)


        def empezar(self):


                self.doNothing=False
                self.image_sub=rospy.Subscriber('/turtlebot3/camera/image_raw',
                                 Image, self.imagen_callback)


        def parar(self):

                self.doNothing = True
                self.twist.linear.x = 0

                self.twist.angular.z = 0
                self.cmd_vel_pub.publish(self.twist)
                #self.image_sub=None
                #self.image=None




                #cv2.waitKey(1)
                #del self
'''

def movebase_client():
        
        # Create an action client called "move_base" with action definition file "MoveBaseAction"
        client = actionlib.SimpleActionClient('move_base', MoveBaseAction)

        # Waits until the action server has started up and started listening for goals.
        client.wait_for_server()

        # Creates a new goal with the MoveBaseGoal constructor
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()
        # Move 0.5 meters forward along the x axis of the "map" coordinate frame
        goal.target_pose.pose.position.x = 0.5
        # No rotation of the mobile base frame w.r.t. map frame
        goal.target_pose.pose.orientation.w = 1.0

        # Sends the goal to the action server.
        client.send_goal(goal)
        # Waits for the server to finish performing the action.
        wait = client.wait_for_result()
        # If the result doesn't arrive, assume the Server is not available
        if not wait:
                rospy.logerr("Action server not available!")
                rospy.signal_shutdown("Action server not available!")
        else:
                # Result of executing the action
                return client.get_result()
'''



def find_marker(image):



        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        rojo_bajo = (0, 186, 100)
        rojo_alto = (25, 255, 255)
        mask = cv2.inRange(hsv, rojo_bajo, rojo_alto)

        # obtenemos la altura, anchura y profundidad
        h, w, d = image.shape

        M = cv2.moments(mask, False)

        try:
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
        except ZeroDivisionError:
                cy, cx = h / 2, w / 2

        # Buscarmos los contornos de los objetos rojos de la imagen capturada
        z,cnts,zz = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)


        # compute the bounding box of the of the paper region and return it
        return cv2.minAreaRect(numpy.array(cnts[0]))



def distance_to_camera(knownWidth, focalLength, perWidth):
	# compute and return the distance from the maker to the camera
	return (knownWidth * focalLength) / perWidth



ARM_DISTANCE=0.2

KNOWN_DISTANCE = 1.891637*39.3701#pasar de metros a inches

KNOWN_WIDTH = 0.1*39.3701
image = cv2.imread(str(sys.path[0])[:-3] + "imagenes/imagenRef.png")  # "/home/gti/catkin_ws/src/seguir_pelota/imagenes/imagenRef.png")
marker = find_marker(image)
focalLength = (marker[1][0] * KNOWN_DISTANCE) / KNOWN_WIDTH

follower=SeguidorPelota(KNOWN_WIDTH, focalLength, ARM_DISTANCE)

def callback(request):
        print("Hola")

        global follower



        print(sys.path[0])

        if(request.arrancar==1):



                follower.empezar()
                rospy.spin()

                try:
                        rospy.spin()
                except KeyboardInterrupt:
                        print("Fin del programa!")

                cv2.destroyAllWindows()

        else:
                follower.parar()


        return True

rospy.init_node('seguir_pelota')

myService = rospy.Service('/jugar', MyCustomServiceMessage, callback) #iniciamos el servicio

rate=rospy.Rate(1)
rospy.loginfo("Service /jugar ready")
rospy.spin() # mantiene el servicio abierto

#if __name__ == '__main__':
 #       main()


