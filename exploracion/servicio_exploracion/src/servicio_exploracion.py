#! /usr/bin/env python

import rospy
import roslaunch
from exploration_msg.srv import MyCustomServiceMessage, MyCustomServiceMessageResponse

import time
import subprocess

def my_callback(request):  # Funcion que se ejecuta cuando se llama al servicio
    segundos=request.segundos
    try:

        p = subprocess.Popen(['roslaunch', 'lanzadera', 'lanzadera.launch', 'segundos:={}'.format(segundos)], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.stdin.close()

        response = MyCustomServiceMessageResponse()

        response.success = True
        return response
    except:
        print("You fucked up")


rospy.init_node('servicio_exploracion') # se inicializa el nodo
myService = rospy.Service('/explorar', MyCustomServiceMessage, my_callback) #iniciamos el servicio

rate=rospy.Rate(1)
rospy.loginfo("Service /servicio_exploracion ready")
rospy.spin() # mantiene el servicio abierto
