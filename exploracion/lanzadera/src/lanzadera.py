#! /usr/bin/env python
#<include file="$(find navigation_stage)/launch/move_base_gmapping_5cm.launch"/>
#  <include file="$(find explore_lite)/launch/explore.launch"/>
import rospy
import time
import roslaunch
import sys
import subprocess

segundos = sys.argv[1]
launch = None
launch2 = None


def abrirRviz():
    global launch
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(uuid)

    cli_args1 = ['navigation_stage', 'move_base_gmapping_5cm.launch']

    launch = roslaunch.scriptapi.ROSLaunch()

    roslaunch_file1 = roslaunch.rlutil.resolve_launch_arguments(cli_args1)

    launch.parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file1)

    # parent.shutdown()


def explorar():
    global launch2
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(uuid)

    cli_args3 = ['explore_lite', 'explore.launch']

    launch2 = roslaunch.scriptapi.ROSLaunch()

    roslaunch_file3 = roslaunch.rlutil.resolve_launch_arguments(cli_args3)
    launch2.parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file3)


abrirRviz()
explorar()
launch.start()
launch2.start()

rospy.sleep(int(segundos))

#launch.parent.shutdown()
#launch2.parent.shutdown()



#subprocess.Popen(['roslaunch','servicio_exploracion', 'servicio_exploracion.launch'])