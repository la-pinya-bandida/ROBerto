#! /usr/bin/env python

import rospy
import time
from smach import State, StateMachine
from smach_ros import IntrospectionServer
import actionlib
from std_msgs.msg import String
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from sonidos.reproducir import reproducir_wav, reproducir_text_to_speech, parar_wav
from text_commands import main
from geometry_msgs.msg import Twist


cmd_vel_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)


class Encender(State):
    def __init__(self):
        State.__init__(self, outcomes=['succeeded', 'aborted'])

    def execute(self, userdata):
        rospy.loginfo("El robot se enciende")
        time.sleep(0)
        return 'succeeded'


class Moverse(State):

    def __init__(self, position, orientation, place):
        State.__init__(self, outcomes=['succeeded', 'aborted'])
        self._orientation = orientation
        self._position = position
        self._place = place
        self._move_base = actionlib.SimpleActionClient("/move_base", MoveBaseAction)
        rospy.loginfo("Accediendo al topic move_base para navegar")
        self._move_base.wait_for_server(rospy.Duration(15))


    def execute(self, userdata):
        rospy.loginfo("Moviendonos a el punto {}".format(self._position))

        goal = MoveBaseGoal() # Creamos el goal
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.pose.position.x = self._position[0] # Insertamos la coordenadas del goal
        goal.target_pose.pose.position.y = self._position[1]
        goal.target_pose.pose.position.z = self._position[2]
        goal.target_pose.pose.orientation.x = self._orientation[0]
        goal.target_pose.pose.orientation.y = self._orientation[1]
        goal.target_pose.pose.orientation.z = self._orientation[2]
        goal.target_pose.pose.orientation.w = self._orientation[3]

        rospy.loginfo("ROBOT %s" %(self._place))
        # sends the goal
        self._move_base.send_goal(goal)
        self._move_base.wait_for_result()
        # Comprobamos el estado de la navegacion
        nav_state = self._move_base.get_state()
        rospy.loginfo("[Result] State: %d" % (nav_state))
        nav_state = 3

        if nav_state == 3:
            return 'succeeded'
        else:
            return 'aborted'


class Hacer_Algo(State):
    def __init__(self, tiempo):
        State.__init__(self, outcomes=['succeeded','aborted'])
        self.tiempo = tiempo

    def execute(self, userdata):
        lista = [None] * self.tiempo
        for cosa in lista:
            time.sleep(1)
            print("Han pasado {} segundos haciendo nada".format(cosa))
        return 'succeeded'


class Reproducir_sonido(State):
    def __init__(self, action, file):
        State.__init__(self, outcomes=['succeeded', 'aborted'])
        self.action = action
        self.file = file

    def execute(self, userdata):
        if self.action == 1:
            reproducir_wav(self.file)

        if self.action == 2:
            print("Paramos el wav")
            parar_wav(self.file)

        if self.action == 3:
            palabras = raw_input("Escribe lo que quieres que el robot diga: ")
            reproducir_text_to_speech(palabras)
        return 'succeeded'


class Iniciar_comunicacion_por_texto(State):
    def __init__(self):
        State.__init__(self, outcomes=['succeeded','aborted'])
        self.text = ""

    def execute(self, userdata):
        main()
        return 'succeeded'


class Moverse_en_circulos(State):
    def __init__(self):
        State.__init__(self, outcomes=['succeeded','aborted'])
        self.text = ""

    def execute(self, userdata):
        twist = Twist()
        twist.linear.x = 0.1
        twist.angular.z = 0.5
        cmd_vel_publisher.publish(twist)
        distance = 1.5;
        while not rospy.is_shutdown():

            t0 = rospy.Time.now().to_sec()
            current_distance = 0

            while (current_distance < distance):
                # Publish the velocity
                cmd_vel_publisher.publish(twist)
                # Takes actual time to velocity calculus
                t1 = rospy.Time.now().to_sec()
                # Calculates distancePoseStamped
                current_distance = twist.linear.x * (t1 - t0)
            # After the loop, stops the robot
            twist.linear.x = 0
            twist.angular.z = 0
            # Force the robot to stop
            cmd_vel_publisher.publish(twist)
            return 'succeeded'




