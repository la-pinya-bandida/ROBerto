#! /usr/bin/env python

import rospy
from smach import State, StateMachine
from smach_ros import IntrospectionServer
from estados import Encender, Moverse, Hacer_Algo, Reproducir_sonido, Iniciar_comunicacion_por_texto, Moverse_en_circulos
from maquina_estados.srv import service_message, service_messageResponse

# ----------------------------------------------------------------------------------------------------------------------
# Autor: Manuel Mouzo Marsella
# ----------------------------------------------------------------------------------------------------------------------



class main():
    def __init__(self):
        rospy.init_node('maquina_node', anonymous=False)

        servicio = rospy.Service('/maquina_estados', service_message, self.callback_maquina_estados)
        # ( "nombre a llamar con rosservice", "el mensaje srv", "el callback" )
        # EJ de llamada: $ rosservice call definir_direction "direction: 1"
        # Desde html es distinto

        rospy.loginfo("Servicio /maquina_estados listo")
        rospy.spin()    # lo que evita que se dejen de escuchar las peticiones rosservices)

    def callback_maquina_estados(self, request):
        maquina = StateMachine(outcomes=['succeeded', 'aborted'])
        maquina.userdata.sm_input = 1

        message = request.message
        A = request.A
        B = request.B
        C = request.C
        print("message: {} A: {} B: {} C: {}".format(message, A, B, C))
        puntos = [
            ['Go_to_xy', (float(A), float(B), 0.0), (0.0, 0.0, 1.0, 0.0)],
        ]

        # "Reproduce el wav"
        if (message == 0):
            with maquina:
                StateMachine.add('Encenderse', Encender(), transitions={'succeeded': 'sonido', 'aborted': 'sonido'})
                StateMachine.add('sonido', Reproducir_sonido(1, C), transitions={'succeeded': 'succeeded', 'aborted': 'aborted'})

        # "Parar el wav"
        if (message == 1):
            with maquina:
                StateMachine.add('Encenderse', Encender(), transitions={'succeeded': 'sonido', 'aborted': 'sonido'})
                StateMachine.add('sonido', Reproducir_sonido(2, ""), transitions={'succeeded': 'succeeded', 'aborted': 'aborted'})

        # "Text-to-speech"
        if (message == 2):
            with maquina:
                StateMachine.add('Encenderse', Encender(), transitions={'succeeded': 'sonido', 'aborted': 'sonido'})
                StateMachine.add('sonido', Reproducir_sonido(3, ""), transitions={'succeeded': 'succeeded', 'aborted': 'aborted'})

        # Ir a un punto en el map
        if (message == 3):
            with maquina:
                StateMachine.add('Encenderse', Encender(), transitions={'succeeded': 'moverBot_XY', 'aborted': 'bad_end'})
                StateMachine.add('moverBot_XY', Moverse(puntos[0][1], puntos[0][2], puntos[0][0]), transitions={'succeeded': 'good_end', 'aborted': 'bad_end'})
                StateMachine.add('good_end', Reproducir_sonido(1, "happy_robot_sound"), transitions={'succeeded': 'succeeded', 'aborted': 'bad_end'})
                StateMachine.add('bad_end', Reproducir_sonido(1, "sad_robot_sound"), transitions={'succeeded': 'succeeded', 'aborted': 'aborted'})

        # Dar vueltas sobre si mismo y cantar
        if (message == 4):
            with maquina:
                StateMachine.add('Encenderse', Encender(), transitions={'succeeded': 'sonido', 'aborted': 'bad_end'})
                StateMachine.add('sonido', Reproducir_sonido(1, C), transitions={'succeeded': 'girar', 'aborted': 'bad_end'})
                StateMachine.add('girar', Moverse_en_circulos(), transitions={'succeeded': 'good_end', 'aborted': 'bad_end'})
                StateMachine.add('good_end', Reproducir_sonido(1, "happy_robot_sound"), transitions={'succeeded': 'succeeded', 'aborted': 'bad_end'})
                StateMachine.add('bad_end', Reproducir_sonido(1, "sad_robot_sound"), transitions={'succeeded': 'succeeded', 'aborted': 'aborted'})

        # Comunicacion con lex
        if (message == 5):
            with maquina:
                StateMachine.add('Encenderse', Encender(), transitions={'succeeded': 'despertar', 'aborted': 'apagar'})
                StateMachine.add('despertar', Reproducir_sonido(1, "happy_robot_sound"), transitions={'succeeded': 'hablar', 'aborted': 'apagar'})
                StateMachine.add('hablar', Iniciar_comunicacion_por_texto(), transitions={'succeeded': 'apagar', 'aborted': 'apagar'})
                StateMachine.add('apagar', Reproducir_sonido(1, "sad_robot_sound"), transitions={'succeeded': 'succeeded', 'aborted': 'aborted'})


        intro_server = IntrospectionServer('Ejemplo', maquina, '/SM_ROOT')
        intro_server.start()

        #Ejecutamos la maquina de estados
        print("Ejecutamos la maquina de estados")
        sm_outcome = maquina.execute()
        intro_server.stop()

        response = service_messageResponse()
        response.success = True
        return response

    def shutdown(self):
        rospy.loginf("Parando la ejecucion...")
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        rospy.loginfo(" Interrupcion detectada ")
