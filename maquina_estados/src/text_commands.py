#! /usr/bin/env python

import rospy
import time
import threading
from std_msgs.msg import String
from sonidos.reproducir import reproducir_text_to_speech

text_input_publisher = rospy.Publisher("/text_input", String, queue_size=5)
wake_publisher = rospy.Publisher("/wake_word", String, queue_size=5)

# ----------------------------------------------------------------------------------------------------------------------
# Autor: Manuel Mouzo Marsella
# ----------------------------------------------------------------------------------------------------------------------

class TextInput:
    wake_words = ("jarvis", "turtlebot")

    def __init__(self, wake_publish_rate=5):
        rospy.Subscriber("/text_output", String, self.display_response)
        print("INICIANDO COMUNICACION")
        self.wake_publish_rate = wake_publish_rate
        wake_thread = threading.Thread(name='wake', target=self.keep_robot_awake)
        wake_thread.daemon = True
        wake_thread.start()

    def keep_robot_awake(self):
        if self.wake_publish_rate == 0:
            return
        while True:
            wake_publisher.publish(self.wake_words[0])
            time.sleep(self.wake_publish_rate)

    def send_text(self, text):
        if text in self.wake_words:
            wake_publisher.publish(text)
            time.sleep(0.1)
        text_input_publisher.publish(text)

    def display_response(self, data):
        text = data.data
        reproducir_text_to_speech(text)
        print(text)

    def get_input(self):
        text = raw_input("")
        if(text == "exit"):
            return
        if len(text) > 0:
            self.send_text(text)
        self.get_input()


def main():
    # Esto deberia de aparecer en forma de pop up o similar al iniciar el chat con el bot:
    usage = """
Usage:
move <forward|backward> <speed>             - Move at meters per second. E.g, "move forward 0.3"
turn <clockwise|counterclockwise> <speed>   - Turn at radians per second. E.g, "turn clockwise 0.78"
stop                                        - Stop all movement
"""
    print(usage)
    text_input = TextInput()
    text_input.get_input()
    print("CERRANDO COMUNICACION")



