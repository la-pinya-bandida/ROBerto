#! /usr/bin/env python

import rospy
from sound_play.libsoundplay import SoundClient
import csv
import os.path

# ----------------------------------------------------------------------------------------------------------------------
# Autor: Manuel Mouzo Marsella
# ----------------------------------------------------------------------------------------------------------------------

sound_client = SoundClient()
my_path = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(my_path, "../../audios/")


def reproducir_text_to_speech(text):
    # Reproducir text-to-speech
    sound_client.say(text)

def reproducir_wav(file):
    sound = sound_client.waveSound(path + file + ".wav")
    sound.play()

def parar_wav(file):
    sound = sound_client.waveSound(path + file + ".wav")
    sound.stop()